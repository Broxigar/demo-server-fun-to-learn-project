var mysql = require('mysql');
var config = require('./config');
// console.log(config);
var db = mysql.createPool({
  connectionLImit: 100,
  host: config.host,
  port: config.port,
  user: config.user,
  password: config.password,
  database: config.database
});

db.getConnection(function(err) {
  if (err) {
    console.log("DB connect error ...");
    console.log(err);
    throw err;
  }
});

db.services = function(callback){
  db.query('SELECT * from API', function(
    err, rows) {
      // console.log('Err: ', err);
    // console.log('The solution is: ', rows);
    callback(err, rows);
  });
}
db.saveService = function(data, callback){
  db.query('insert into API set ?',[data], function(
    err, rows) {
      console.log('Err: ', err);
    console.log('The solution is: ', rows);
    callback(err, rows);
  });
}

module.exports = db;
